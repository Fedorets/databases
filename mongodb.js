var express = require('express');
var app = express();
let MongoClient = require('mongodb').MongoClient;
let assert = require("assert");
let url = "mongodb://localhost:27017/mydb";
let data;
MongoClient.connect(url, function (err,db) {
   assert.equal(null, err);
   db.createCollection('students', function (err, res) {
       if(err) throw err;
       db.close();
   });

    let arrayPeople = [{
            name: 'qwertyMan',
            age: '35',
            groups: '1'
        },{
            name: 'Elena',
            age: '20',
            groups: '2'
        },{
            name: 'Nick',
            age: '35',
            groups: '43'
        },{
            name: 'Sonya',
            age: '30',
            groups: '10'
        },{
            name: 'Pops',
            age: '3',
            groups: '5'
        }
    ];
    //
    // db.collection('students').insertMany(arrayPeople, function (err, res) {
    //     if(err) throw err;
    //     console.log('Added!');
    //     db.close();
    // });

    // db.collection('students').insertOne({
    //     name: 'Po2',
    //     age: '55',
    //     groups: '66'}, function (err, res) {
    //         if(err) throw err;
    //         console.log('Added!');
    // });

    // db.collection('students').updateOne(
    //         { "name" : "Sonya" },
    //         { $set: { "age" : 33 }
    //     }, function (err, res) {
    //         if(err) throw err;
    //         console.log('Update!');
    //     });

    // db.collection('students').drop();

    // db.collection('students').deleteOne({ "name" : "Pops" },
    //      function (err, res) {
    //         if(err) throw err;
    //         console.log('Delete!');
    //         // db.close();
    //     });

    db.collection('students').find({}).toArray(function (err, res) {
        if(err) throw err;
        data = res;
        console.log('Select!');
        db.close();
    });
   console.log("Correctly connect to server.");
});

app.get('/', function (request, response) {
    response.send(data);
});

app.listen(3002);
