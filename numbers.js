var numbers = [3,7,1,5,2,7,9,10,2,65,2];

function calc() {
    var mas = {
        even: [],
        odd: []
    };
    for (var i of numbers) {
        i % 2 === 0 ? mas.even.push(i) : mas.odd.push(i);
    }
    return mas;
}

module.exports = {
    calc: calc
};