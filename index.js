var express = require('express');
var app = express();
var path = require("path");
let table;

let calc = require('./numbers');

console.log(calc.calc());

// app.get('/', function(req, res) {
//    res.sendFile(path.join(__dirname + '/index.html'));
// });

app.get('/', function (request, response) {
    // response.send(table);

    response.send(`<table>
                        <tr>
                            <td>id</td>
                            <td>name</td>
                            <td>age</td>
                            <td>group</td>
                        </tr>
                        ${rowFromTable(table)}
                    </table>`)
});

function rowFromTable(table) {
   let arr = [];
    for(let i in table) {
       arr.push(`<tr>
                   <td> ${table[i].id} </td>
                   <td> ${table[i].name} </td>
                   <td> ${table[i].age} </td>
                   <td> ${table[i].groups} </td>
               </tr>`)
    }
    return arr;
}

let mysql = require('mysql');
let con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb'
});

con.connect(function (err) {
    if(err) throw err;
    console.log('Connected');
    // con.query('CREATE TABLE students (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), age INT, groups INT)', function (err, result) {
    //     if(err) throw err;
    //     console.log('Table created');
    // });

    // con.query("INSERT INTO students(name, age, groups) VALUES ('Nick', 34, 5)", function (err, result) {
    //     console.log('User created');
    // });

    con.query('SELECT * FROM students', function (err, result) {
        table = result;
    });
});

app.listen(3001);